//
//  AddressesViewController.swift
//  ExerciseMapKit
//
//  Created by Rahul Rawat on 09/06/21.
//

import UIKit
import MapKit

class AddressesViewController: BaseViewController {

    @IBOutlet weak var lbCurrentAddress: UILabel!
    @IBOutlet weak var lbSearchedAddress: UILabel!
    
    private var currentLocation: CLLocationCoordinate2D? = nil
    private var searchedLocation: CLLocationCoordinate2D? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lbCurrentAddress.lineBreakMode = .byWordWrapping
        lbCurrentAddress.numberOfLines = 0
        
        lbSearchedAddress.lineBreakMode = .byWordWrapping
        lbSearchedAddress.numberOfLines = 0

        reverseGeocode(location: currentLocation) { [weak self] placemark in
            self?.lbCurrentAddress?.text = placemark.getAddress()
            
            self?.reverseGeocode(location: self?.searchedLocation) { [weak self] placemark in
                self?.lbSearchedAddress?.text = placemark.getAddress()
            }
        }
    }
    
    func initialiseLocations(currentLocation: CLLocationCoordinate2D?, searchedLocation: CLLocationCoordinate2D?) {
        self.currentLocation = currentLocation
        self.searchedLocation = searchedLocation
    }
}
