//
//  MapViewController.swift
//  ExerciseMapKit
//
//  Created by Rahul Rawat on 09/06/21.
//

import UIKit
import MapKit

class MapViewController: BaseViewController {
    
    @IBOutlet weak var mapView: MKMapView!
    
    private var currentLocationAnnotation: MKPointAnnotation? = nil
    private var searchedLocationAnnotation: MKPointAnnotation? = nil
    
    private var currentLocation: CLLocationCoordinate2D? = nil
    private var searchedLocation: CLLocationCoordinate2D? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        locationActionTappedOnce = true
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Info", style: .plain, target: self, action: #selector(infoTapped))
        
        if let annotation = self.searchedLocationAnnotation {
            mapView?.addAnnotation(annotation)
        }
        
        zoomToAllAnnotations()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        locateMe(locationManager: locationManager)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        locationManager.stopUpdatingLocation()
    }
    
    override func locationPermissionGranted() {
        mapView.showsUserLocation = true
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toAdressVC" {
            let vc = segue.destination as! AddressesViewController
            vc.initialiseLocations(currentLocation: currentLocation, searchedLocation: searchedLocation)
        }
    }
    
    func setSearchedLocation(withSearchText searchText: String, withLocation location: CLLocationCoordinate2D) {
        searchedLocation = location
        
        searchedLocationAnnotation = MKPointAnnotation()
        searchedLocationAnnotation?.title = searchText
        searchedLocationAnnotation?.coordinate = location
    }
    
    @objc func infoTapped(_ sender: UIButton) {
        performSegue(withIdentifier: "toAdressVC", sender: self)
    }
    
    override func stopFetchingOnceFetched() -> Bool {
        false
    }
        
    override func updateLocation(withLocation location: CLLocation) {
        currentLocation = location.coordinate
        
        zoomToAllAnnotations()
    }
    
    private func zoomToAllAnnotations() {
        if currentLocation != nil && searchedLocation != nil {
           fitAll()
        } else if currentLocation != nil {
            zoomToCoordinates(toCoordinates: currentLocation!)
        } else {
            zoomToCoordinates(toCoordinates: searchedLocation!)
        }
    }
    
    /// function that tries to show both annotations at same time but will fail if the zoom level required is more than the maximum zoom level of map view
    func fitAll() {
        var zoomRect = MKMapRect.null
        for location in [currentLocation!, searchedLocation!] {
            let mapPoint = MKMapPoint(location)
            let pointRect = MKMapRect(x: mapPoint.x, y: mapPoint.y, width: 0.1, height: 0.1)
            zoomRect = zoomRect.union(pointRect)
        }
        mapView.setVisibleMapRect(zoomRect, edgePadding: UIEdgeInsets(top: 100, left: 100, bottom: 100, right: 100), animated: true)
    }
    
    private func zoomToCoordinates(toCoordinates coordinates: CLLocationCoordinate2D) {
        let span = MKCoordinateSpan(latitudeDelta: 0.1, longitudeDelta: 0.1)
        let region = MKCoordinateRegion(center: coordinates, span: span)
        
        mapView.setRegion(region, animated: true)
    }
}
