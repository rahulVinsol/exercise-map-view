//
//  SearchViewController.swift
//  ExerciseMapKit
//
//  Created by Rahul Rawat on 10/06/21.
//

import UIKit
import MapKit

class SearchViewController: BaseViewController {

    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var btSearch: UIButton!
    
    private var searchText: String!
    private var location: CLLocationCoordinate2D!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        searchBar.delegate = self
        searchBar.becomeFirstResponder()
        
        searchBar.searchTextField.delegate = self
        
        searchBar.layer.borderWidth = 1
        searchBar.layer.borderColor = UIColor.white.cgColor
        
        btSearch.layer.cornerRadius = 10
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toMapSegue" {
            let vc = segue.destination as! MapViewController
            vc.setSearchedLocation(withSearchText: searchText, withLocation: location)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.isNavigationBarHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        navigationController?.isNavigationBarHidden = false
    }
    
    @IBAction func searchTapped(_ sender: UIButton) {
        searchBarSearchButtonClicked(searchBar)
    }
}

extension SearchViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case searchBar.searchTextField:
            searchBarSearchButtonClicked(searchBar)
        default:
            break
        }
        return true
    }
}

extension SearchViewController: UISearchBarDelegate {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        let searchText = searchBar.text
        
        guard searchText?.isEmpty == false else {
            return
        }
        
        self.searchText = searchText
        
        self.view.isUserInteractionEnabled = false
        
        activityIndicator.startAnimating()
        
        searchBar.resignFirstResponder()
        
        let searchRequest = MKLocalSearch.Request()
        searchRequest.naturalLanguageQuery = searchBar.text
        
        let activeSearch = MKLocalSearch(request: searchRequest)
        activeSearch.start { [weak self] (response, error) in
            guard let self = self else { return }
            
            self.view?.isUserInteractionEnabled = true
            self.activityIndicator?.stopAnimating()
            
            if error != nil || response == nil {
                self.showLocationErrorVC(locationError: .SearchError, error: error)
            } else {
                self.location = CLLocationCoordinate2D(latitude: response!.boundingRegion.center.latitude, longitude: response!.boundingRegion.center.longitude)
                
                self.performSegue(withIdentifier: "toMapSegue", sender: self)
            }
        }
    }
}
