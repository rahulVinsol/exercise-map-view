//
//  LocateViewController.swift
//  ExerciseMapKit
//
//  Created by Rahul Rawat on 09/06/21.
//

import UIKit
import CoreLocation

class LocateViewController: BaseViewController {
    
    @IBOutlet weak var btFindMe: UIButton!
    @IBOutlet weak var lbStreet1: UILabel!
    @IBOutlet weak var lbStreet2: UILabel!
    @IBOutlet weak var lbCountry: UILabel!
    @IBOutlet weak var lbCity: UILabel!
    @IBOutlet weak var lbState: UILabel!
    @IBOutlet weak var lbZipcode: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        updatePlacemark(withPlacemark: nil)
        btFindMe.layer.cornerRadius = 10
        // Do any additional setup after loading the view.
    }
    
    @IBAction func findMe(_ sender: UIButton) {
        locationActionTappedOnce = true
        locateMe(locationManager: locationManager)
    }
    
    override func updateLocation(withLocation location: CLLocation) {
        reverseGeocode(location: location, completionHandler: updatePlacemark(withPlacemark:))
    }
    
    func updatePlacemark(withPlacemark placemark: CLPlacemark?) {
        if let placemark = placemark {
            lbStreet1.text = "Street 1: \(placemark.subThoroughfare.getStringOrDefault())"
            lbStreet2.text = "Street 2: \(placemark.thoroughfare.getStringOrDefault())"
            lbCountry.text = "Country: \(placemark.country.getStringOrDefault())"
            lbCity.text = "City: \(placemark.locality.getStringOrDefault())"
            lbState.text = "State: \(placemark.administrativeArea.getStringOrDefault())"
            lbZipcode.text = "Zip code: \(placemark.postalCode.getStringOrDefault())"
        } else {
            lbStreet1.text = "Street 1: N/A"
            lbStreet2.text = "Street 2: N/A"
            lbCountry.text = "Country: N/A"
            lbCity.text = "City: N/A"
            lbState.text = "State: N/A"
            lbZipcode.text = "Zip code: N/A"
        }
    }
}
