//
//  BaseViewController.swift
//  ExerciseMapKit
//
//  Created by Rahul Rawat on 09/06/21.
//

import UIKit
import CoreLocation

enum LocationError {
    case PermissionDenied, Restricted, Error, GeocodingError, SearchError
}

class BaseViewController: UIViewController {
    
    let locationManager = CLLocationManager()
    let geocoder = CLGeocoder()
    
    open var locationActionTappedOnce = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        locationManager.delegate = self
        // Do any additional setup after loading the view.
    }
    
    func updateLocation(withLocation location: CLLocation) {
        
    }
    
    func stopFetchingOnceFetched() -> Bool {
        true
    }
    
    func reverseGeocode(location: CLLocationCoordinate2D?, completionHandler:((_: CLPlacemark?)->Void)? = nil) {
        guard location != nil else {
            completionHandler?(nil)
            return
        }
        return reverseGeocode(location: CLLocation(latitude: location!.latitude, longitude: location!.longitude), completionHandler: completionHandler)
    }
    
    func reverseGeocode(location: CLLocation?, completionHandler: ((_: CLPlacemark?)->Void)? = nil) {
        guard location != nil else {
            completionHandler?(nil)
            return
        }
        
        geocoder.reverseGeocodeLocation(location!, completionHandler:  { [weak self] (placemarks, error) in
            if error == nil && placemarks?.isEmpty == false {
                completionHandler?(placemarks!.first)
            } else if error != nil {
                self?.showLocationErrorVC(locationError: .GeocodingError, error: error)
            } else {
                self?.showLocationErrorVC(locationError: .GeocodingError)
            }
        })
    }
    
    func locationPermissionGranted() {  }
}

extension BaseViewController: CLLocationManagerDelegate {
    
    func locateMe(locationManager: CLLocationManager) {
        if !locationActionTappedOnce {
            return
        }
        let authStatus = locationManager.authorizationStatus
        switch authStatus {
        case .notDetermined:
            locationManager.requestWhenInUseAuthorization()
            return
        case .denied:
            showLocationErrorVC(locationError: .PermissionDenied)
            return
        case .restricted:
            showLocationErrorVC(locationError: .Restricted)
            return
        default:
            break
        }
        locationManager.startUpdatingLocation()
        locationPermissionGranted()
    }
    
    func showLocationErrorVC(locationError: LocationError, error: Error? = nil) {
        var title: String? = nil
        var message: String? = nil
        var addSettingsAction = false
        
        switch locationError {
        case .PermissionDenied:
            title = "Permission Denied"
            message = "Please provide permission after going to settings"
            addSettingsAction = true
        case .Restricted:
            title = "Restricted"
            message = "Cannot fetch location. It seems device is restricted"
        case .Error:
            title = "Error"
            message = "Location Fetching Failed"
        case .GeocodingError:
            title = "Geocoding Error"
            message = "Error reverse geocoding location"
        case .SearchError:
            title = "Search Error"
            message = "Cannot find search results"
        }
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        if addSettingsAction {
            alert.addAction(UIAlertAction(title: "Settings", style: .default, handler: { action in
                if let url = URL(string: UIApplication.openSettingsURLString) {
                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                }
            }))
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        } else {
            alert.addAction(UIAlertAction(title: "Okay", style: .default, handler: nil))
        }
        present(alert, animated: true, completion: nil)
    }
    
    func locationManagerDidChangeAuthorization(_ manager: CLLocationManager) {
        locateMe(locationManager: manager)
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        switch (error as NSError).code {
        case CLError.Code.locationUnknown.rawValue, CLError.Code.headingFailure.rawValue :
            break
        case CLError.Code.denied.rawValue :
            showLocationErrorVC(locationError: .Error, error: error)
            locationManager.stopUpdatingLocation()
        default:
            break
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.first else {
            return
        }
        
        updateLocation(withLocation: location)
        
        if stopFetchingOnceFetched() {
            locationManager.stopUpdatingLocation()
        }
    }
}
