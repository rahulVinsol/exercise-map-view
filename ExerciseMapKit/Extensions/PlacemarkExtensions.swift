//
//  PlacemarkExtensions.swift
//  ExerciseMapKit
//
//  Created by Rahul Rawat on 09/06/21.
//

import MapKit

extension Optional where Wrapped == CLPlacemark {
    func getAddress() -> String {
        guard let self = self else {
            return "N/A"
        }
        var address = ""
        
        if let text = self.subThoroughfare {
            address += "\(text), "
        }
        if let text = self.thoroughfare {
            address += "\(text), "
        }
        if let text = self.country {
            address += "\(text), "
        }
        if let text = self.locality {
            address += "\(text), "
        }
        if let text = self.administrativeArea {
            address += "\(text), "
        }
        if let text = self.postalCode {
            address += "\(text)"
        }
        
        return address == "" ? "N/A" : address
    }
}
