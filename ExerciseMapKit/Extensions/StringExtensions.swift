//
//  StringExtensions.swift
//  ExerciseMapKit
//
//  Created by Rahul Rawat on 09/06/21.
//

import Foundation

extension Optional where Wrapped == String {
    func getStringOrDefault(default: String = "N/A") -> String {
        self == nil ? `default` : self.unsafelyUnwrapped
    }
}
